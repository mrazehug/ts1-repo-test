package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static cz.cvut.fel.ts1.Mrazehug.*;

public class MrazehugTest {
    @Test
    public void factorialTest()
    {
        Mrazehug m = new Mrazehug();
        try {
            assertEquals(m.factorial(5), 120);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
